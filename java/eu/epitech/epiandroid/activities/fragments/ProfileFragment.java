package eu.epitech.epiandroid.activities.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import eu.epitech.epiandroid.R;
import eu.epitech.epiandroid.activities.adapters.MessagesAdapter;
import eu.epitech.epiandroid.handlers.MessageEventsHandler;
import eu.epitech.epiandroid.handlers.UserEventsHandler;
import eu.epitech.epiandroid.models.Message;
import eu.epitech.epiandroid.models.User;
import eu.epitech.epiandroid.services.MessageService;
import eu.epitech.epiandroid.services.UserService;

public class ProfileFragment extends Fragment implements MessageEventsHandler, UserEventsHandler
{
    private MessagesAdapter messageListAdapter = null;
    private ArrayAdapter<String> infoListAdapter = null;
    private ImageView photoImageView = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        MessageService messageService = new  MessageService(this);
        messageService.messageList();
        UserService userService = new  UserService(this);
        userService.infos();
        userService.photo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_profil, container, false);

        ArrayList<Message> message = new ArrayList<Message>();
        messageListAdapter = new MessagesAdapter(getActivity(), message);
        ListView messageListView = (ListView) view.findViewById(R.id.messagesList);
        messageListView.setAdapter(messageListAdapter);

        ListView infoListView = (ListView) view.findViewById(R.id.infosList);
        infoListAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1);
        infoListView.setAdapter(infoListAdapter);

        photoImageView = (ImageView) view.findViewById(R.id.imageProfil);
        return view;
    }

    @Override
    public void onSelfInfosSuccess(User user)
    {
        List<String> userInfos = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("#.00");

        userInfos.add(user.getFirstname() + " " + user.getLastname());
        userInfos.add(getString(R.string.login) + " " + user.getLogin());
        userInfos.add(getString(R.string.promotion) + " " + user.getPromo());
        userInfos.add(getString(R.string.log) + " " + df.format(user.getActiveLog()));

        infoListAdapter.addAll(userInfos);
        infoListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSelfInfosError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onInfosSuccess(User user)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onInfosError(String error)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onPhotoSuccess(String photo)
    {
        Picasso.with(getActivity())
                .load(photo)
                .into(photoImageView);
    }

    @Override
    public void onPhotoError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMessageListSuccess(Message[] messages)
    {
        messageListAdapter.addAll(messages);
        messageListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMessageListError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTrombiSuccess(User[] users)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onTrombiError(String error)
    {
        throw new UnsupportedOperationException();
    }
}
