package eu.epitech.epiandroid.activities.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;

import eu.epitech.epiandroid.R;
import eu.epitech.epiandroid.activities.adapters.ModulesAdapter;
import eu.epitech.epiandroid.handlers.ModuleEventsHandler;
import eu.epitech.epiandroid.models.Module;
import eu.epitech.epiandroid.services.ModuleService;

public class ModulesFragment extends Fragment implements ModuleEventsHandler
{
    private ModulesAdapter moduleListAdapter = null;
    private boolean mySelfSwitch = false;
    private ArrayList<Module> modulesList = new ArrayList<Module>();
    private ArrayList<Module> mySelfModulesList = new ArrayList<Module>();
    private ModuleService moduleService;
    public View view;

    private boolean[] listCheckbox = new boolean[6];


    public void subscribe(Module module)
    {
        moduleService.subscribe(module);
    }

    public void unsubscribe(Module module)
    {
        moduleService.unsubscribe(module);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        moduleService = new  ModuleService(this);
        moduleService.moduleList(2018, "FR/STG");
    }

    @Override
    public void onModuleSubscribeSuccess(Module module)
    {
        moduleService.moduleList(2018, "FR/STG");
    }

    @Override
    public void onModuleSubscribeError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onModuleUnsubscribeSuccess(Module module)
    {
        moduleService.moduleList(2018, "FR/STG");
    }

    @Override
       public void onSelfModuleListSuccess(Module[] modules)
    {
        ArrayList<Module> tmpmodule = new ArrayList<Module>();
        for (Module module : modules) {
            if (module != null && module.getTitle() != null) {
                tmpmodule.add(module);
            }
        }

        mySelfModulesList.clear();
        mySelfModulesList = tmpmodule;
        checkRegister();
    }

    @Override
    public void onSelfModuleListError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onModuleUnsubscribeError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    public void checkRegister()
    {
        for (Module module : modulesList) {
            for (Module module2 : mySelfModulesList)
            {
                if (module.getId() == module2.getId()) {
                    module.setStudentRegistered(true);
                    if (module2.getStudentGrade() != null)
                        module.setStudentGrade(module2.getStudentGrade());
                }
            }
        }
        moduleListAdapter.clear();
        moduleListAdapter.addAll(semesterModulesList(modulesList));
        moduleListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onModuleListSuccess(Module[] modules)
    {
        ArrayList<Module> tmpmodule = new ArrayList<Module>();
        for (Module module : modules) {
            if (module != null && module.getTitle() != null) {
                tmpmodule.add(module);
            }
        }
        modulesList.clear();
        modulesList = tmpmodule;
        moduleService.selfModuleList();
    }

    @Override
    public void onModuleListError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    public ArrayList<Module> selfModulesList(ArrayList<Module> modules)
    {
        ArrayList<Module> tmpmodule = new ArrayList<Module>();
        for (Module module : modules) {
            if (module.isStudentRegistered()) {
                tmpmodule.add(module);
            }
        }

        return tmpmodule;
    }

    public ArrayList<Module> semesterModulesList(ArrayList<Module> modules)
    {
        ArrayList<Module> tmpmodule = new ArrayList<Module>();
        for (Module module : modules) {
            for (int i = 0; i < 6; i++)
                if (module.getSemester() == i && listCheckbox[i])
                tmpmodule.add(module);
        }

        return tmpmodule;
    }


    private String getGrades()
    {
        String message = "";
        for (Module module : mySelfModulesList) {
            if (module.getStudentGrade() != null && !module.getStudentGrade().equals("-"))
            {
                message += module.getTitle() + ": grade " + module.getStudentGrade() + "\n";
            }
        }
        return (message);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_modules, container, false);

        ArrayList<Module> module = new ArrayList<Module>();
        moduleListAdapter = new ModulesAdapter(getActivity(), module, ModulesFragment.this);

        ListView moduleListView = (ListView) view.findViewById(R.id.modulesList);
        moduleListView.setAdapter(moduleListAdapter);

        FloatingActionButton button = (FloatingActionButton) view.findViewById(R.id.grade_share);

        button.setOnClickListener(new CompoundButton.OnClickListener() {
            public void onClick(View v) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);

                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_TEXT, getGrades());

                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
        });

        final Switch SelfSwitch = (Switch) view.findViewById(R.id.switchMySelf);
        SelfSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mySelfSwitch = isChecked;
                moduleListAdapter.clear();
                if (isChecked)
                    moduleListAdapter.addAll(semesterModulesList(selfModulesList(modulesList)));
                else
                    moduleListAdapter.addAll(semesterModulesList(modulesList));
                moduleListAdapter.notifyDataSetChanged();
            }

        });

        for (int i = 0; i < 6; i++)
        {
            listCheckbox[i] = true;
            String checkboxID = "checkBoxB"+i;
            int resID = getResources().getIdentifier(checkboxID, "id", "eu.epitech.epiandroid");
            final CheckBox box = (CheckBox) view.findViewById(resID);
            box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    String text = buttonView.getText().toString();
                    int i = Integer.parseInt(text.replaceAll("[\\D]", ""));
                    if (isChecked) {
                        listCheckbox[i] = true;
                    } else {
                        listCheckbox[i] = false;
                    }
                    moduleListAdapter.clear();
                    if (mySelfSwitch)
                        moduleListAdapter.addAll(semesterModulesList(selfModulesList(modulesList)));
                    else
                        moduleListAdapter.addAll(semesterModulesList(modulesList));
                    moduleListAdapter.notifyDataSetChanged();
                }

            });
        }
        return view;
    }
}
