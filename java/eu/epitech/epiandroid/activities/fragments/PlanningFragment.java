package eu.epitech.epiandroid.activities.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import eu.epitech.epiandroid.R;
import eu.epitech.epiandroid.activities.adapters.PlanningEventsAdapter;
import eu.epitech.epiandroid.handlers.PlanningEventsHandler;
import eu.epitech.epiandroid.models.Event;
import eu.epitech.epiandroid.services.PlanningService;
import lombok.Getter;

public class PlanningFragment extends Fragment implements PlanningEventsHandler
{
    private ListView eventListView = null;
    private PlanningEventsAdapter eventListAdapter = null;

    @Getter
    private List<Event> events = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        PlanningService planningService = new PlanningService(this);
        planningService.eventList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_planning, container, false);

        eventListView = (ListView) view.findViewById(R.id.eventList);
        eventListAdapter = new PlanningEventsAdapter(getActivity(), android.R.layout.simple_list_item_1);
        eventListView.setAdapter(eventListAdapter);
        eventListView.setOnItemClickListener(new PlanningListener());

        FloatingActionButton shareBtn = (FloatingActionButton) view.findViewById(R.id.planningShareBtn);
        shareBtn.setOnClickListener(new ShareListener(this));

        return view;
    }

    @Override
    public void onEventListSuccess(Event[] events)
    {
        Arrays.sort(events, new Comparator<Event>() {
            public int compare(Event e1, Event e2)
            {
                if (e1.getStartDateTime().isBefore(e2.getStartDateTime()))
                    return (-1);
                else if (e1.getStartDateTime().isAfter(e2.getStartDateTime()))
                    return (1);
                else
                    return (0);
            }
        });

        List<Event> displayedEvents = new ArrayList<>();
        for (Event event : events)
        {
            if (event.isRegisteredToModule())
                displayedEvents.add(event);
        }

        this.events = displayedEvents;

        eventListAdapter.clear();
        eventListAdapter.addAll(displayedEvents);
        eventListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onEventListError(String error)
    {
        System.out.println(error);
    }

    @Override
    public void onEventSubscribeError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEventSubscribeSuccess(Event event)
    {
        PlanningService planningService = new PlanningService(this);
        planningService.eventList();
    }

    @Override
    public void onEventUnsubscribeError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEventUnsubscribeSuccess(Event event)
    {
        PlanningService planningService = new PlanningService(this);
        planningService.eventList();
    }

    @Override
    public void onTokenSubmitSuccess(Event event)
    {
        Toast.makeText(getActivity(), getText(R.string.planning_token_validated), Toast.LENGTH_SHORT).show();

        PlanningService planningService = new PlanningService(this);
        planningService.eventList();
    }

    @Override
    public void onTokenSubmitError(String error) {
        Toast.makeText(getActivity(), getText(R.string.planning_token_error) + " : " + error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * Class handling the various planning event's possible actions
     */
    private class PlanningListener implements AdapterView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            PlanningEventsAdapter.ViewHolder viewHolder = (PlanningEventsAdapter.ViewHolder) view.getTag();

            if (viewHolder.event != null)
            {
                if (view.getId() == R.id.eventAddToPlanningBtn)
                {
                    Event event = viewHolder.event;

                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setType("vnd.android.cursor.item/event");
                    intent.putExtra("title", event.getTitle());
                    intent.putExtra("description", "");
                    intent.putExtra("beginTime", event.getStartDateTime().getMillis());
                    intent.putExtra("endTime", event.getEndDateTime().getMillis());
                    startActivity(intent);
                }
                else
                {
                    PlanningService planningService = new PlanningService(PlanningFragment.this);
                    switch (viewHolder.event.getState()) {
                        case CAN_REGISTER:
                            planningService.subscribe(viewHolder.event);
                            break;
                        case CAN_UNREGISTER:
                            planningService.unsubscribe(viewHolder.event);
                            break;
                        case WAITING_TOKEN:
                            planningService.submitToken(viewHolder.event, viewHolder.eventTokenInput.getText().toString());
                            break;
                        default:
                            Log.d("Planning event", "Unsupported operation");
                            break;
                    }
                }
            }
        }
    }

    /**
     * Class handling the "share" event
     */
    private class ShareListener implements View.OnClickListener
    {
        private final PlanningFragment fragment;

        public ShareListener(PlanningFragment fragment)
        {
            super();

            this.fragment = fragment;
        }

        @Override
        public void onClick(View v)
        {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);

            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_TEXT, listToString(fragment.getEvents()));

            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        }

        private String listToString(List<Event> events)
        {
            String output = "";

            int i = 0;
            for (Event event : events)
            {
                if (!event.isPast())
                {
                    output += event.getTitle() + " - " + event.getStartDate() + "\n\n";
                    if (i++ >= 5)
                        break;
                }
            }

            return (output);
        }
    }

}
