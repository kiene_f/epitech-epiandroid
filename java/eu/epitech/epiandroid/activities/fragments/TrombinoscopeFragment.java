package eu.epitech.epiandroid.activities.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import eu.epitech.epiandroid.R;
import eu.epitech.epiandroid.activities.adapters.TrombinoscopeAdapter;
import eu.epitech.epiandroid.handlers.UserEventsHandler;
import eu.epitech.epiandroid.models.User;
import eu.epitech.epiandroid.services.UserService;

public class TrombinoscopeFragment extends Fragment implements UserEventsHandler {

    private TrombinoscopeAdapter trombinoscopeListAdapter = null;
    private UserService userService = null;
    private String location = "FR/PAR";
    private String promo = "tek1";
    private Dialog options;
    private User currentUser = null;

    @Override
    public void onSelfInfosSuccess(User user)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onSelfInfosError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onInfosSuccess(User user)
    {
        currentUser = user;
        options.show();
    }

    @Override
    public void onInfosError(String error)
    {
        System.out.println(error);
    }

    @Override
    public void onPhotoSuccess(String photo)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onPhotoError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTrombiSuccess(User[] users)
    {
        trombinoscopeListAdapter.clear();
        trombinoscopeListAdapter.addAll(users);
        trombinoscopeListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onTrombiError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userService = new  UserService(this);
        userService.trombi(getString(R.string.year), location, promo);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_trombinoscope, container, false);

        ArrayList<User> users = new ArrayList<>();
        trombinoscopeListAdapter = new TrombinoscopeAdapter(getActivity(), users, TrombinoscopeFragment.this);

        ListView trombinoscopeListView = (ListView) view.findViewById(R.id.trombinoscope_list);
        trombinoscopeListView.setAdapter(trombinoscopeListAdapter);

        trombinoscopeListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id)
            {
                User user = (User) parent.getItemAtPosition(pos);
                userService.infos(user.getLogin());
            }
        });

        Spinner locationSpinner = (Spinner) view.findViewById(R.id.location_spinner);
        ArrayAdapter<CharSequence> locationAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.location_array, android.R.layout.simple_spinner_item);
        locationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locationSpinner.setAdapter(locationAdapter);

        locationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
            {
                location = parent.getItemAtPosition(pos).toString();
                userService.trombi(getString(R.string.year), location, promo);
            }

            public void onNothingSelected(AdapterView<?> parent) {}
        });

        Spinner promoSpinner = (Spinner) view.findViewById(R.id.promo_spinner);
        ArrayAdapter<CharSequence> promoAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.promo_array, android.R.layout.simple_spinner_item);
        promoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        promoSpinner.setAdapter(promoAdapter);

        promoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
            {
                promo = parent.getItemAtPosition(pos).toString();
                userService.trombi(getString(R.string.year), location, promo);
            }

            public void onNothingSelected(AdapterView<?> parent) {}
        });

         options = new AlertDialog.Builder(getContext(), AlertDialog.THEME_HOLO_LIGHT)
                .setTitle(getString(R.string.options_name))
                .setNegativeButton(getString(R.string.cancel), null)
                .setItems(new String[]{getString(R.string.option_1), getString(R.string.option_2), getString(R.string.option_3)}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dlg, int position) {
                        if (position == 0) {
                            Intent intent = new Intent(Intent.ACTION_INSERT);
                            intent.setType(ContactsContract.Contacts.CONTENT_TYPE);

                            intent.putExtra(ContactsContract.Intents.Insert.NAME, currentUser.getFullname());
                            if (currentUser.getPhone() != null)
                                intent.putExtra(ContactsContract.Intents.Insert.PHONE, currentUser.getPhone());
                            intent.putExtra(ContactsContract.Intents.Insert.EMAIL, currentUser.getEmail());

                            getContext().startActivity(intent);
                        } else if (position == 1 && currentUser.getPhone() != null) {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", currentUser.getPhone(), null));
                            getContext().startActivity(intent);
                        } else if (position == 2) {
                            Intent mailer = new Intent(Intent.ACTION_SEND);
                            mailer.setType("text/plain");
                            mailer.putExtra(Intent.EXTRA_EMAIL, currentUser.getEmail());
                            startActivity(Intent.createChooser(mailer, ""));
                        }
                    }
                })
                .create();

        return (view);
    }

}
