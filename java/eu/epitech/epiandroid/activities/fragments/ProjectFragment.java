package eu.epitech.epiandroid.activities.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import eu.epitech.epiandroid.R;
import eu.epitech.epiandroid.activities.adapters.MessagesAdapter;
import eu.epitech.epiandroid.activities.adapters.PlanningEventsAdapter;
import eu.epitech.epiandroid.activities.adapters.ProjectsAdapter;
import eu.epitech.epiandroid.handlers.ProjectEventHandler;
import eu.epitech.epiandroid.models.Project;
import eu.epitech.epiandroid.services.PlanningService;
import eu.epitech.epiandroid.services.ProjectService;

public class ProjectFragment extends Fragment implements ProjectEventHandler
{
    private ListView projectListView = null;
    private ProjectsAdapter projectsListAdapter = null;
    private ProjectService projectService = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        projectService = new ProjectService(this);
        projectService.projectList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_project, container, false);

        projectListView = (ListView) view.findViewById(R.id.projectsList);
        projectsListAdapter = new ProjectsAdapter(getActivity(), android.R.layout.simple_list_item_1);
        projectListView.setAdapter(projectsListAdapter);
        projectListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ProjectsAdapter.ViewHolder viewHolder = (ProjectsAdapter.ViewHolder) view.getTag();

                if (viewHolder.project != null) {
                    ProjectService projectService = new ProjectService(ProjectFragment.this);
                    switch (viewHolder.project.isRegistered() == true ? 1 : 0) {
                        case 1:
                            projectService.unsubscribe(viewHolder.project);
                            break;
                        case 0:
                            projectService.subscribe(viewHolder.project);
                            break;
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public void onProjectListSuccess(Project[] projects) {
        projectsListAdapter.clear();
        projectsListAdapter.addAll(projects);
        projectsListAdapter.notifyDataSetChanged();
    }

    public void onProjectlistError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    public void onProjectSubscribeSuccess(Project project)
    {
        projectService = new ProjectService(this);
        projectService.projectList();
    }

    public void onProjectSubscribeError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    public void onProjectUnsubscribeSuccess(Project event)
    {
        projectService = new ProjectService(this);
        projectService.projectList();
    }

    public void onProjectUnsubscribeError(String error)
    {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
