package eu.epitech.epiandroid.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import eu.epitech.epiandroid.R;
import eu.epitech.epiandroid.activities.fragments.TrombinoscopeFragment;
import eu.epitech.epiandroid.models.User;

/**
 * Created by François on 22/01/2016.
 */
public class TrombinoscopeAdapter extends ArrayAdapter<User> {

    public TrombinoscopeAdapter(Context context, ArrayList<User> modules, TrombinoscopeFragment fragment) {
        super(context, 0, modules);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        User user = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_trombinoscope_list, parent, false);
        }

        TextView login = (TextView) convertView.findViewById(R.id.trombi_login);
        TextView name = (TextView) convertView.findViewById(R.id.trombi_name);
        ImageView image = (ImageView) convertView.findViewById(R.id.trombi_image);

        login.setText(user.getLogin());
        name.setText(user.getFullname());
        Picasso.with(getContext())
                .load(user.getPicture())
                .into(image);

        return convertView;
    }
}