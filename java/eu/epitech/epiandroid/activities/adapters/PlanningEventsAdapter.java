package eu.epitech.epiandroid.activities.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;

import eu.epitech.epiandroid.R;
import eu.epitech.epiandroid.models.Event;

public class PlanningEventsAdapter extends ArrayAdapter<Event>
{
    private HashMap<Integer, Boolean> activeItems = new HashMap<>();
    private List<Event> events;

    public PlanningEventsAdapter(Context context, int resource)
    {
        super(context, resource);
    }

    public PlanningEventsAdapter(Context context, int resource, List<Event> events)
    {
        super(context, resource, events);

        this.events = events;
    }

    public static class ViewHolder
    {
        public Event event;
        public TextView eventTitle;
        public LinearLayout eventInformations;
            public TextView eventDate;
            public LinearLayout eventControlsWrapper;
                public ImageView eventControl;
                public Button eventAddToPlanningBtn;
        public LinearLayout eventPopup;
            public LinearLayout eventPopupRegister;
                public Button eventRegisterBtn;
            public LinearLayout eventPopupUnregister;
                public Button eventUnregisterBtn;
            public LinearLayout eventPopupToken;
                public EditText eventTokenInput;
                public Button eventTokenBtn;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent)
    {
        final ViewHolder viewHolder;

        Event event = getItem(position);
        if (convertView == null)
        {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.adapter_planning_events, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.eventTitle = (TextView) convertView.findViewById(R.id.eventTitle);
            viewHolder.eventInformations = (LinearLayout) convertView.findViewById(R.id.eventInformations);
            viewHolder.eventDate = (TextView) convertView.findViewById(R.id.eventDate);
            viewHolder.eventControlsWrapper = (LinearLayout) convertView.findViewById(R.id.eventControlsWrapper);
            viewHolder.eventControl = (ImageView) convertView.findViewById(R.id.eventControl);
            viewHolder.eventAddToPlanningBtn = (Button) convertView.findViewById(R.id.eventAddToPlanningBtn);
            viewHolder.eventPopup = (LinearLayout) convertView.findViewById(R.id.eventPopup);
            viewHolder.eventPopupRegister = (LinearLayout) convertView.findViewById(R.id.eventPopupRegister);
            viewHolder.eventRegisterBtn = (Button) convertView.findViewById(R.id.eventRegisterBtn);
            viewHolder.eventPopupUnregister = (LinearLayout) convertView.findViewById(R.id.eventPopupUnregister);
            viewHolder.eventUnregisterBtn = (Button) convertView.findViewById(R.id.eventUnregisterBtn);
            viewHolder.eventPopupToken = (LinearLayout) convertView.findViewById(R.id.eventPopupToken);
            viewHolder.eventTokenBtn = (Button) convertView.findViewById(R.id.eventTokenBtn);
            viewHolder.eventTokenInput = (EditText) convertView.findViewById(R.id.eventTokenInput);
            viewHolder.eventAddToPlanningBtn.setTag(viewHolder);
            viewHolder.eventRegisterBtn.setTag(viewHolder);
            viewHolder.eventUnregisterBtn.setTag(viewHolder);
            viewHolder.eventTokenBtn.setTag(viewHolder);

            convertView.setTag(viewHolder);
            convertView.setTag(R.id.eventTitle, viewHolder.eventTitle);
            convertView.setTag(R.id.eventInformations, viewHolder.eventInformations);
            convertView.setTag(R.id.eventDate, viewHolder.eventDate);
            convertView.setTag(R.id.eventControlsWrapper, viewHolder.eventControlsWrapper);
            convertView.setTag(R.id.eventAddToPlanningBtn, viewHolder.eventAddToPlanningBtn);
            convertView.setTag(R.id.eventControl, viewHolder.eventControl);
            convertView.setTag(R.id.eventPopup, viewHolder.eventPopup);
            convertView.setTag(R.id.eventRegisterBtn, viewHolder.eventRegisterBtn);
            convertView.setTag(R.id.eventUnregisterBtn, viewHolder.eventUnregisterBtn);
            convertView.setTag(R.id.eventTokenBtn, viewHolder.eventTokenBtn);
            convertView.setTag(R.id.eventTokenInput, viewHolder.eventTokenInput);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ListView) parent).performItemClick(v, position, 0);
                }
            };
            viewHolder.eventAddToPlanningBtn.setOnClickListener(listener);
            viewHolder.eventRegisterBtn.setOnClickListener(listener);
            viewHolder.eventUnregisterBtn.setOnClickListener(listener);
            viewHolder.eventTokenBtn.setOnClickListener(listener);
        }
        else
            viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.event = event;

        if (event != null)
        {
            viewHolder.eventTitle.setText(event.getTitle());
            viewHolder.eventDate.setText(event.getStartDate());

            switch (event.getState())
            {
                case CAN_REGISTER:
                    viewHolder.eventControl.setImageResource(R.drawable.sign_add);
                    viewHolder.eventPopupRegister.setVisibility(View.VISIBLE);
                    viewHolder.eventPopupUnregister.setVisibility(View.GONE);
                    viewHolder.eventPopupToken.setVisibility(View.GONE);
                    break;
                case CAN_UNREGISTER:
                    viewHolder.eventControl.setImageResource(R.drawable.sign_stop);
                    viewHolder.eventPopupRegister.setVisibility(View.GONE);
                    viewHolder.eventPopupUnregister.setVisibility(View.VISIBLE);
                    viewHolder.eventPopupToken.setVisibility(View.GONE);
                    break;
                case WAITING_TOKEN:
                    viewHolder.eventControl.setImageResource(R.drawable.token_needed);
                    viewHolder.eventPopupRegister.setVisibility(View.GONE);
                    viewHolder.eventPopupUnregister.setVisibility(View.GONE);
                    viewHolder.eventPopupToken.setVisibility(View.VISIBLE);
                    break;
                case REGISTERED:
                    viewHolder.eventControl.setImageResource(R.drawable.sign_stop_disabled);
                    viewHolder.eventPopupRegister.setVisibility(View.GONE);
                    viewHolder.eventPopupUnregister.setVisibility(View.GONE);
                    viewHolder.eventPopupToken.setVisibility(View.GONE);
                    break;
                case VALIDATED:
                    viewHolder.eventControl.setImageResource(R.drawable.sign_check);
                    viewHolder.eventPopupRegister.setVisibility(View.GONE);
                    viewHolder.eventPopupUnregister.setVisibility(View.GONE);
                    viewHolder.eventPopupToken.setVisibility(View.GONE);
                    break;
                case FAILED:
                    viewHolder.eventControl.setImageResource(R.drawable.sign_error);
                    viewHolder.eventPopupRegister.setVisibility(View.GONE);
                    viewHolder.eventPopupUnregister.setVisibility(View.GONE);
                    viewHolder.eventPopupToken.setVisibility(View.GONE);
                    break;
                default:
                    viewHolder.eventControl.setImageResource(0);
                    viewHolder.eventPopupRegister.setVisibility(View.GONE);
                    viewHolder.eventPopupUnregister.setVisibility(View.GONE);
                    viewHolder.eventPopupToken.setVisibility(View.GONE);
                    break;
            }
        }
        viewHolder.eventPopup.setTag(position);

        if (activeItems.containsKey(position) && activeItems.get(position))
            viewHolder.eventPopup.setVisibility(View.VISIBLE);
        else
            viewHolder.eventPopup.setVisibility(View.GONE);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewHolder viewHolder1 = (ViewHolder) v.getTag();
                activeItems.put((int) viewHolder1.eventPopup.getTag(), !activeItems.containsKey((int) viewHolder1.eventPopup.getTag()) || !activeItems.get((int) viewHolder1.eventPopup.getTag()));

                if (activeItems.get((int) viewHolder1.eventPopup.getTag()))
                    viewHolder1.eventPopup.setVisibility(View.VISIBLE);
                else
                    viewHolder1.eventPopup.setVisibility(View.GONE);
            }
        });

        return (convertView);
    }
}
