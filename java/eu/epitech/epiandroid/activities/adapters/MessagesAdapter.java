package eu.epitech.epiandroid.activities.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import eu.epitech.epiandroid.R;
import eu.epitech.epiandroid.models.Message;

/**
 * Created by François on 22/01/2016.
 */
public class MessagesAdapter extends ArrayAdapter<Message> {
    public MessagesAdapter(Context context, ArrayList<Message> messages) {
        super(context, 0, messages);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Message message = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_message_list, parent, false);
        }

        TextView title = (TextView) convertView.findViewById(R.id.list_title);
        TextView content = (TextView) convertView.findViewById(R.id.list_content);

        if (message != null) {
            title.setText(Html.fromHtml(message.getTitle()));
            content.setText(Html.fromHtml(message.getContent()));
        }

        return convertView;
    }
}
