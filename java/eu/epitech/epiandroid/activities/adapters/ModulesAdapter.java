package eu.epitech.epiandroid.activities.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import eu.epitech.epiandroid.R;
import eu.epitech.epiandroid.activities.fragments.ModulesFragment;
import eu.epitech.epiandroid.models.Module;

/**
 * Created by François on 22/01/2016.
 */
public class ModulesAdapter extends ArrayAdapter<Module> {
    private ModulesFragment fragment;

    public ModulesAdapter(Context context, ArrayList<Module> modules, ModulesFragment fragment) {
        super(context, 0, modules);
        this.fragment = fragment;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Module module = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_module_list, parent, false);
        }

        TextView title = (TextView) convertView.findViewById(R.id.list_title);
        TextView content = (TextView) convertView.findViewById(R.id.list_content);
        Switch register = (Switch) convertView.findViewById(R.id.switch_register);
        final TextView registerLabel = (TextView) convertView.findViewById(R.id.switch_register_label);

        register.setTag(position);
        register.setOnCheckedChangeListener(null);
        register.setChecked(module.isStudentRegistered());
        register.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                int position = (int) buttonView.getTag();
                Module module = getItem(position);
                if (isChecked) {
                    registerLabel.setText(getContext().getString(R.string.register));
                    if (module.canRegister())
                        fragment.subscribe(module);
                } else {
                    registerLabel.setText(getContext().getString(R.string.unregister));
                    if (module.canUnregister())
                        fragment.unsubscribe(module);
                }
            }
        });

        title.setText(module.getTitle());
        String ct = "";
        if (module.isStudentRegistered())
        {
            registerLabel.setText(getContext().getString(R.string.register));
            register.setChecked(true);
        }
        else {
            registerLabel.setText(getContext().getString(R.string.unregister));
            register.setChecked(false);
        }

        if (module.getStudentGrade() != null && !module.getStudentGrade().equals("-")) {
            registerLabel.setVisibility(View.GONE);
            register.setVisibility(View.GONE);
            ct = getContext().getString(R.string.grade) + " " + module.getStudentGrade();
        }
        else if  (!module.canRegister() && !module.isStudentRegistered()) {
            registerLabel.setVisibility(View.GONE);
            register.setVisibility(View.GONE);
            ct = getContext().getString(R.string.unregister);
        }
        else if (!module.canUnregister() && module.isStudentRegistered()) {
            registerLabel.setVisibility(View.GONE);
            register.setVisibility(View.GONE);
            ct = getContext().getString(R.string.register);
        }
        else {
            registerLabel.setVisibility(View.VISIBLE);
            register.setVisibility(View.VISIBLE);
        }
        content.setText(ct);

        return convertView;
    }
}
