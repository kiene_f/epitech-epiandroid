package eu.epitech.epiandroid.activities.adapters;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import eu.epitech.epiandroid.R;
import eu.epitech.epiandroid.activities.fragments.ProjectFragment;
import eu.epitech.epiandroid.models.Event;
import eu.epitech.epiandroid.models.Project;

public class ProjectsAdapter extends ArrayAdapter<Project>
{

    public ProjectsAdapter(Context context, int resource) {
        super(context, resource);
    }

    public ProjectsAdapter(Context context, int resource, List<Project> projects) {
        super(context, resource, projects);
    }

    public static class ViewHolder
    {
        public Project project;
        public TextView projectTitle;
        public TextView projectStart;
        public TextView projectEnd;
        public Button projectRegistrationBtn;
        public Button projectUnregistrationBtn;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent)
    {
        final ViewHolder viewHolder;

        final Project project = getItem(position);

        if (convertView == null)
        {
            LayoutInflater li = LayoutInflater.from(getContext());
            convertView = li.inflate(R.layout.adapter_projects_list, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.projectTitle = (TextView) convertView.findViewById(R.id.projectTitle);
            viewHolder.projectStart = (TextView) convertView.findViewById(R.id.projectStart);
            viewHolder.projectEnd = (TextView) convertView.findViewById(R.id.projectEnd);
            viewHolder.projectRegistrationBtn = (Button) convertView.findViewById(R.id.projectRegistrationBtn);
            viewHolder.projectUnregistrationBtn = (Button) convertView.findViewById(R.id.projectUnregistrationBtn);
            viewHolder.projectRegistrationBtn.setTag(viewHolder);
            viewHolder.projectUnregistrationBtn.setTag(viewHolder);

            convertView.setTag(viewHolder);
            convertView.setTag(R.id.projectTitle, viewHolder.projectTitle);
            convertView.setTag(R.id.projectStart, viewHolder.projectStart);
            convertView.setTag(R.id.projectEnd, viewHolder.projectEnd);
            convertView.setTag(R.id.projectRegistrationBtn, viewHolder.projectRegistrationBtn);
            convertView.setTag(R.id.projectUnregistrationBtn, viewHolder.projectUnregistrationBtn);

            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ListView) parent).performItemClick(v, position, 0);
                }
            };
            viewHolder.projectRegistrationBtn.setOnClickListener(listener);
            viewHolder.projectUnregistrationBtn.setOnClickListener(listener);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.project = project;

        if (project != null)
        {
            viewHolder.projectRegistrationBtn.setVisibility(View.GONE);
            viewHolder.projectUnregistrationBtn.setVisibility(View.GONE);

            if (project.isRegistered() && project.isInProgress())
                viewHolder.projectUnregistrationBtn.setVisibility(View.VISIBLE);
            else if (project.isInProgress())
                viewHolder.projectRegistrationBtn.setVisibility(View.VISIBLE);

            viewHolder.projectTitle.setText(Html.fromHtml(project.getTitle()));
            viewHolder.projectStart.setText(getContext().getString(R.string.project_start) + " : " + project.getStart());
            viewHolder.projectEnd.setText(getContext().getString(R.string.project_end) + " : " + project.getEnd());
        }

        return convertView;
    }
}
