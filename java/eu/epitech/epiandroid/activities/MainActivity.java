package eu.epitech.epiandroid.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import eu.epitech.epiandroid.R;
import eu.epitech.epiandroid.activities.fragments.ModulesFragment;
import eu.epitech.epiandroid.activities.fragments.PlanningFragment;
import eu.epitech.epiandroid.activities.fragments.ProfileFragment;
import eu.epitech.epiandroid.activities.fragments.ProjectFragment;
import eu.epitech.epiandroid.activities.fragments.TrombinoscopeFragment;
import eu.epitech.epiandroid.handlers.UserEventsHandler;

import eu.epitech.epiandroid.models.User;
import eu.epitech.epiandroid.services.UserService;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        UserEventsHandler
{

    private DrawerLayout mDrawer;
    private Toolbar toolbar;

    protected void setFragment()
    {
        Fragment fragment = null;

        Class fragmentClass;
        fragmentClass = ProfileFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();
        mDrawer.closeDrawers();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setFragment();

        UserService userService = new  UserService(this);
        userService.infos();
        userService.photo();
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        selectDrawerItem(menuItem);
        return true;
    }


    public void selectDrawerItem(MenuItem item) {

        Fragment fragment = null;

        Class fragmentClass;
        switch (item.getItemId()) {
            case R.id.nav_modules_fragment:
                fragmentClass = ModulesFragment.class;
                break;
            case R.id.nav_planning_fragment:
                fragmentClass = PlanningFragment.class;
                break;
            case R.id.nav_profil_fragment:
                fragmentClass = ProfileFragment.class;
                break;
            case R.id.nav_trombinoscope_fragment:
                fragmentClass = TrombinoscopeFragment.class;
                break;
            case R.id.nav_project_fragment:
                fragmentClass = ProjectFragment.class;
                break;
            default:
                fragmentClass = ProfileFragment.class;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).addToBackStack(null).commit();

        item.setChecked(true);
        setTitle(item.getTitle());
        mDrawer.closeDrawers();
    }

    @Override
    public void onSelfInfosSuccess(User user)
    {
        TextView name = (TextView) findViewById(R.id.menu_name);
        TextView email = (TextView) findViewById(R.id.menu_email);

        String tmp = user.getFirstname() + " " + user.getLastname();
        name.setText(tmp);
        email.setText(user.getEmail());
    }

    @Override
    public void onSelfInfosError(String error)
    {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPhotoSuccess(String photo)
    {
        Picasso.with(this)
                .load(photo)
                .into((ImageView) findViewById(R.id.menu_imageView));
    }

    @Override
    public void onPhotoError(String error)
    {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onInfosSuccess(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onInfosError(String error) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onTrombiSuccess(User[] users) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onTrombiError(String error) {
        throw new UnsupportedOperationException();
    }
}
