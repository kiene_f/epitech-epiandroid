package eu.epitech.epiandroid.models;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import lombok.Getter;
import lombok.Setter;

public class Module
{
    static DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd");

    @Getter @Setter
    @SerializedName("id")
    private int id = -1;

    // Redirige vers id à cause des JSON pourris
    @Getter @Setter
    @SerializedName("id_instance")
    private String instanceId = null;

    @Getter @Setter
    @SerializedName("scolaryear")
    private int scolarYear;

    @Getter @Setter
    private int semester;

    @Getter @Setter
    @SerializedName("codemodule")
    private String moduleCode = null;

    // Redirige vers moduleCode à cause des JSON pourris
    @Getter @Setter
    @SerializedName("code")
    private String code = null;

    @Getter @Setter
    @SerializedName("codeinstance")
    private String instanceCode;

    @Getter @Setter
    private String title;

    @Getter @Setter
    @SerializedName("grade")
    private String studentGrade;

    @Getter @Setter
    private int studentCredits;

    @Getter @Setter
    @SerializedName("student_registered")
    private boolean studentRegistered = false;

    @Getter @Setter
    @SerializedName("end_register")
    private String registrationLimit;

    @Getter @Setter
    @SerializedName("open")
    private String open;

    public String getModuleCode()
    {
        if (moduleCode == null)
            moduleCode = code;
        else if (code == null)
            code = moduleCode;
        return (moduleCode);
    }

    public String getCode()
    {
        if (moduleCode == null)
            moduleCode = code;
        else if (code == null)
            code = moduleCode;
        return (code);
    }

    public int getId()
    {
        if (id == -1)
            id = Integer.parseInt(instanceId);
        else if (instanceId == null)
            instanceId = Integer.toString(id);
        return (id);
    }

    public String getInstanceId()
    {
        if (id == -1)
            id = Integer.parseInt(instanceId);
        else if (instanceId == null)
            instanceId = Integer.toString(id);
        return (instanceId);
    }

    public boolean canRegister()
    {
        if (registrationLimit != null)
        {
            DateTime dt = dateTimeFormatter.parseDateTime(registrationLimit);

            return (dt.isAfter(new DateTime()) && !isStudentRegistered() && isOpen());
        }
        else
            return (false);
    }

    public boolean canUnregister()
    {
        if (registrationLimit != null) {
            DateTime dt = dateTimeFormatter.parseDateTime(registrationLimit);

            return (dt.isAfter(new DateTime()) && isStudentRegistered() && isOpen());
        }
        else
            return (false);
    }

    public boolean isOpen()
    {
        return (open.equals("1"));
    }
}
