package eu.epitech.epiandroid.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

public class User
{
    @Getter @Setter
    private String login;

    @Getter @Setter
    private String lastname;

    @Getter @Setter
    private String firstname;

    @Getter @Setter
    @SerializedName("title")
    private String fullname;

    @Getter @Setter
    @SerializedName("internal_email")
    private String email;

    @Getter @Setter
    private String phone = null;

    @Getter @Setter
    private int promo;

    @Getter @Setter
    private int semester;

    @Getter @Setter
    private String location;

    @Getter @Setter
    private double activeLog;

    @Getter @Setter
    private String picture;
}
