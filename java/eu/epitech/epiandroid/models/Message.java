package eu.epitech.epiandroid.models;

import org.joda.time.DateTime;


import lombok.Getter;
import lombok.Setter;

public class Message
{
    public Message(String t, String c)
    {
        title = t;
        content = c;
    }

    @Getter
    @Setter
    private String title;

    @Getter @Setter
    private String content;

    /*
    @Getter @Setter
    private DateTime date;
    */
}
