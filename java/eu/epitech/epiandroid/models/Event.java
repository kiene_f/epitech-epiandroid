package eu.epitech.epiandroid.models;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import lombok.Getter;
import lombok.Setter;

public class Event
{
    static DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    @Getter @Setter
    private int semester;

    @Getter @Setter
    @SerializedName("codeinstance")
    private String instanceCode;

    @Getter @Setter
    @SerializedName("codeevent")
    private String eventCode;

    @Getter @Setter
    @SerializedName("codeacti")
    private String activityCode;

    @Getter @Setter
    @SerializedName("codemodule")
    private String moduleCode;

    @Getter @Setter
    @SerializedName("allow_token")
    private boolean tokenAllowed;

    @Getter @Setter
    @SerializedName("allow_register")
    private boolean registrationAllowed;

    @Getter @Setter
    @SerializedName("module_registered")
    private boolean registeredToModule;

    @Getter @Setter
    @SerializedName("past")
    private boolean past;

    @Getter @Setter
    @SerializedName("project")
    private boolean project;

    @Getter @Setter
    @SerializedName("scolaryear")
    private int scolarYear;

    @Getter @Setter
    @SerializedName("start")
    private String startDate;

    @Getter @Setter
    @SerializedName("end")
    private String endDate;

    @Getter @Setter
    @SerializedName("acti_title")
    private String title;

    @Getter @Setter
    @SerializedName("type_code")
    private String activityType;

    @Getter @Setter
    @SerializedName("event_registered")
    private String userRegistered;

    public DateTime getStartDateTime()
    {
        DateTime dt = dateTimeFormatter.parseDateTime(startDate);
        return (dt);
    }

    public DateTime getEndDateTime()
    {
        DateTime dt = dateTimeFormatter.parseDateTime(endDate);
        return (dt);
    }

    public boolean isUserRegistered()
    {
        return (!userRegistered.equals("false"));
    }

    public enum RegistrationStatus
    {
        UNREGISTERED,
        REGISTERED,
        FAILED,
        PRESENT,
    }
    public RegistrationStatus getRegistrationStatus()
    {
        RegistrationStatus status;

        switch (userRegistered)
        {
            case "registered":
                status = RegistrationStatus.REGISTERED;
                break;
            case "failed":
                status = RegistrationStatus.FAILED;
                break;
            case "present":
                status = RegistrationStatus.PRESENT;
                break;
            default:
                status = RegistrationStatus.UNREGISTERED;
                break;
        }

        return (status);
    }

    public enum EventState
    {
        UNDEFINED,
        CAN_REGISTER,
        CAN_UNREGISTER,
        WAITING_TOKEN,
        REGISTERED,
        VALIDATED,
        FAILED
    }
    public EventState getState()
    {
        EventState state = EventState.UNDEFINED;

        if ((getRegistrationStatus() == RegistrationStatus.PRESENT)
                || (getRegistrationStatus() == RegistrationStatus.REGISTERED && isPast() && isProject()))
        {
            state = EventState.VALIDATED;
        }
        else if (getRegistrationStatus() == RegistrationStatus.FAILED)
        {
            state = EventState.FAILED;
        }
        else if (getRegistrationStatus() == RegistrationStatus.UNREGISTERED && isRegisteredToModule()
                && isRegistrationAllowed() && !isPast())
        {
            state = EventState.CAN_REGISTER;
        }
        else if (getRegistrationStatus() == RegistrationStatus.REGISTERED && isRegistrationAllowed() && !isPast())
        {
            state = EventState.CAN_UNREGISTER;
        }
        else if (getRegistrationStatus() == RegistrationStatus.REGISTERED && isTokenAllowed() && !isProject() && isPast())
        {
            state = EventState.WAITING_TOKEN;
        }
        else if (getRegistrationStatus() == RegistrationStatus.REGISTERED && !isRegistrationAllowed())
        {
            state = EventState.REGISTERED;
        }

        return (state);
    }

}
