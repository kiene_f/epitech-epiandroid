package eu.epitech.epiandroid.models;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import lombok.Getter;
import lombok.Setter;

public class Project
{
    static DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    @Getter @Setter
    @SerializedName("scolaryear")
    private int scolarYear;

    @Getter @Setter
    @SerializedName("codemodule")
    private String moduleCode;

    @Getter @Setter
    @SerializedName("codeinstance")
    private String instanceCode;

    @Getter @Setter
    @SerializedName("codeacti")
    private String activityCode;

    @Getter @Setter
    @SerializedName("acti_title")
    private String title;

    @Getter @Setter
    @SerializedName("registered")
    private int registered;

    @Getter @Setter
    @SerializedName("type_acti_code")
    private String type;

    @Getter @Setter
    @SerializedName("begin_acti")
    private String start;

    @Getter @Setter
    @SerializedName("end_acti")
    private String end;

    public boolean isRegistered()
    {
        return (registered != 0);
    }

    public boolean isInProgress()
    {
        DateTime start = dateTimeFormatter.parseDateTime(this.start);
        DateTime end = dateTimeFormatter.parseDateTime(this.end);
        DateTime now = new DateTime();

        return (start.isBefore(now) && end.isAfter(now));
    }
}
