package eu.epitech.epiandroid.services;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import eu.epitech.epiandroid.handlers.ProjectEventHandler;
import eu.epitech.epiandroid.models.Event;
import eu.epitech.epiandroid.models.Project;

public class ProjectService extends ServiceBase
{
    final private ProjectEventHandler projectEventHandler;

    public ProjectService(ProjectEventHandler projectEventHandler)
    {
        this.projectEventHandler = projectEventHandler;
    }

    public void projectList()
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());

        apiService.get("/projects", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    Project[] projects = gson.fromJson(response, Project[].class);

                    List<Project> projectsList = new ArrayList<Project>();
                    for (Project project : projects) {
                        if (project.getType().equals("proj")) {
                            projectsList.add(project);
                        }
                    }

                    projectEventHandler.onProjectListSuccess(projectsList.toArray(new Project[projectsList.size()]));
                } catch (Exception e) {
                    projectEventHandler.onProjectlistError("Error while processing json: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response) {
                if (response != null && isJSONValid(response)) {
                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(response).getAsJsonObject();
                        JsonObject error = obj.getAsJsonObject("error");
                        String message = error.getAsJsonPrimitive("message").getAsString();
                        projectEventHandler.onProjectlistError(message);
                    } catch (Exception e) {
                        projectEventHandler.onProjectlistError(response);
                    }
                } else
                    projectEventHandler.onProjectlistError(response);
            }
        });
    }

    public void subscribe(final Project project)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("scolaryear", Integer.toString(project.getScolarYear()));
        params.put("codemodule", project.getModuleCode());
        params.put("codeinstance", project.getInstanceCode());
        params.put("codeacti", project.getActivityCode());

        apiService.post("/project", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    projectEventHandler.onProjectSubscribeSuccess(project);
                } catch (Exception e) {
                    projectEventHandler.onProjectSubscribeError("Error while processing json: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response) {
                if (isJSONValid(response))
                {
                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(response).getAsJsonObject();
                        JsonObject error = obj.getAsJsonObject("error");
                        String message = error.getAsJsonPrimitive("message").getAsString();
                        projectEventHandler.onProjectlistError(message);
                    } catch (Exception e)
                    {
                        projectEventHandler.onProjectlistError(response);
                    }
                } else
                    projectEventHandler.onProjectlistError(response);
            }
        });
    }

    public void unsubscribe(final Project project)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("scolaryear", Integer.toString(project.getScolarYear()));
        params.put("codemodule", project.getModuleCode());
        params.put("codeinstance", project.getInstanceCode());
        params.put("codeacti", project.getActivityCode());


        apiService.delete("/project", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    projectEventHandler.onProjectUnsubscribeSuccess(project);
                } catch (Exception e) {
                    projectEventHandler.onProjectUnsubscribeError("Error while processing json: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response) {
                if (isJSONValid(response))
                {
                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(response).getAsJsonObject();
                        JsonObject error = obj.getAsJsonObject("error");
                        String message = error.getAsJsonPrimitive("message").getAsString();
                        projectEventHandler.onProjectUnsubscribeError(message);
                    } catch (Exception e)
                    {
                        projectEventHandler.onProjectUnsubscribeError(response);
                    }
                } else
                    projectEventHandler.onProjectUnsubscribeError(response);
            }
        });
    }
}
