package eu.epitech.epiandroid.services;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;

import eu.epitech.epiandroid.handlers.AuthEventsHandler;

public class AuthenticationService extends ServiceBase
{
    private final AuthEventsHandler authEventsHandler;

    public AuthenticationService(AuthEventsHandler authEventsHandler)
    {
        this.authEventsHandler = authEventsHandler;
    }

    public void login(String login, String password)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("login", login);
        params.put("password", password);

        apiService.post("/login", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response)
            {
                try
                {
                    AuthenticationManager.Token token = gson.fromJson(response, AuthenticationManager.Token.class);
                    authEventsHandler.onLoginSuccess(token);
                }
                catch (Exception e)
                {
                    authEventsHandler.onLoginError("Error while processing token: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response)
            {
                if (isJSONValid(response))
                {
                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(response).getAsJsonObject();
                        JsonObject error = obj.getAsJsonObject("error");
                        String message = error.getAsJsonPrimitive("message").getAsString();
                        authEventsHandler.onLoginError(message);
                    } catch (Exception e)
                    {
                        authEventsHandler.onLoginError(response);
                    }
                }
                else
                    authEventsHandler.onLoginError(response);
            }
        });
    }
}
