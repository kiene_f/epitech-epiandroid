package eu.epitech.epiandroid.services;

public interface ResponseHandler
{
    void onSuccess(String response);

    void onFailure(String response);
}
