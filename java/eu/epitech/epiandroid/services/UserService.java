package eu.epitech.epiandroid.services;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Properties;

import eu.epitech.epiandroid.handlers.UserEventsHandler;
import eu.epitech.epiandroid.models.User;

public class UserService extends ServiceBase
{
    private final UserEventsHandler userEventsHandler;

    public UserService(UserEventsHandler userEventsHandler)
    {
        this.userEventsHandler = userEventsHandler;
    }

    /**
     * Récupère les informations de l'utilisateur connecté
     */
    public void infos()
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());

        apiService.post("/infos", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();
                    JsonObject userInfos = obj.getAsJsonObject("infos");

                    User user = gson.fromJson(userInfos, User.class);

                    JsonArray userCurrents = obj.getAsJsonArray("current");
                    if (userCurrents != null) {
                        JsonObject userCurrent = userCurrents.get(0).getAsJsonObject();
                        user.setActiveLog(Double.parseDouble(userCurrent.getAsJsonPrimitive("active_log").getAsString()));
                    }

                    AuthenticationManager.setUser(user);
                    userEventsHandler.onSelfInfosSuccess(user);
                } catch (Exception e) {
                    userEventsHandler.onSelfInfosError("Error while processing json: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response) {
                try {
                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();
                    JsonObject error = obj.getAsJsonObject("error");
                    String message = error.getAsJsonPrimitive("message").getAsString();
                    userEventsHandler.onSelfInfosError(message);
                } catch (Exception e) {
                    userEventsHandler.onSelfInfosError(response);
                }
            }
        });
    }

    /**
     * Récupère les informations d'un utilisateur
     * @param login Utilisateur sur lequel les informations seront récupérées
     */
    public void infos(String login)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("user", login);

        apiService.get("/user", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response)
            {
                try
                {
                    User user = gson.fromJson(response, User.class);

                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();

                    user.setEmail(obj.getAsJsonPrimitive("internal_email").getAsString());
                    user.setPicture(obj.getAsJsonPrimitive("picture").getAsString());

                    JsonObject userInfo = obj.getAsJsonObject("userinfo");
                    JsonObject phone = userInfo.getAsJsonObject("telephone");
                    if (phone != null)
                        user.setPhone(phone.getAsJsonPrimitive("value").getAsString());

                    userEventsHandler.onInfosSuccess(user);
                }
                catch (Exception e)
                {
                    userEventsHandler.onInfosError("Error while processing json:" + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response)
            {
                try {
                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();
                    JsonObject error = obj.getAsJsonObject("error");
                    String message = error.getAsJsonPrimitive("message").getAsString();
                    userEventsHandler.onInfosError(message);
                } catch (Exception e)
                {
                    userEventsHandler.onInfosError(response);
                }
            }
        });
    }

    public void trombi(String year, String location, String promo)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("year", year);
        params.put("location", location);
        params.put("promo", promo);

        apiService.get("/trombi", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response)
            {
                try
                {
                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();
                    JsonArray items = obj.getAsJsonArray("items");

                    User[] users = gson.fromJson(items, User[].class);

                    userEventsHandler.onTrombiSuccess(users);
                }
                catch (Exception e)
                {
                    userEventsHandler.onTrombiError("Error while processing json:" + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response)
            {
                try {
                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();
                    JsonObject error = obj.getAsJsonObject("error");
                    String message = error.getAsJsonPrimitive("message").getAsString();
                    userEventsHandler.onTrombiError(message);
                } catch (Exception e)
                {
                    userEventsHandler.onTrombiError(response);
                }
            }
        });
    }

    /**
     * Récupère l'adresse de la photo de l'utilisateur courant
     */
    public void photo()
    {
        this.photo(AuthenticationManager.getUser().getLogin());
    }

    /**
     * Récupère l'adresse de la photo d'un utilisateur
     * @param login Utilisateur dont la photo sera récupérée
     */
    public void photo(String login)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("login", login);

        apiService.get("/photo", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    String url = gson.fromJson(response, Properties.class).getProperty("url");

                    userEventsHandler.onPhotoSuccess(url);
                } catch (Exception e) {
                    userEventsHandler.onPhotoError("Error while processing json:" + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response)
            {
                try {
                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();
                    JsonObject error = obj.getAsJsonObject("error");
                    String message = error.getAsJsonPrimitive("message").getAsString();
                    userEventsHandler.onPhotoError(message);
                } catch (Exception e)
                {
                    userEventsHandler.onPhotoError(response);
                }
            }
        });
    }
}
