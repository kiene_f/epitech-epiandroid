package eu.epitech.epiandroid.services;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;

import eu.epitech.epiandroid.handlers.MessageEventsHandler;
import eu.epitech.epiandroid.models.Message;

/**
 * Gestion des messages d'information affichés sur la page d'accueil de l'utilisateur
 */
public class MessageService extends ServiceBase
{
    private final MessageEventsHandler messageEventsHandler;

    public MessageService(MessageEventsHandler messageEventsHandler)
    {
        this.messageEventsHandler = messageEventsHandler;
    }

    /**
     * Récupère la liste des messages du profil de l'utilisateur
     */
    public void messageList()
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());

        apiService.get("/messages", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response)
            {
                try
                {
                    Message[] messages = gson.fromJson(response, Message[].class);

                    messageEventsHandler.onMessageListSuccess(messages);
                }
                catch (Exception e)
                {
                    messageEventsHandler.onMessageListError("Error while processing json:" + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response)
            {
                try
                {
                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();
                    JsonObject error = obj.getAsJsonObject("error");
                    String message = error.getAsJsonPrimitive("message").getAsString();
                    messageEventsHandler.onMessageListError(message);
                } catch (Exception e)
                {
                    messageEventsHandler.onMessageListError(response);
                }
            }
        });
    }
}
