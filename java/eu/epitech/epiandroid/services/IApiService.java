package eu.epitech.epiandroid.services;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.HashMap;
import java.util.Map;

public interface IApiService
{
    void get(String url, HashMap<String, String> params, final ResponseHandler responseHandler);
    void post(String url, HashMap<String, String> params, final ResponseHandler responseHandler);
    void delete(String url, HashMap<String, String> params, final ResponseHandler responseHandler);
}
