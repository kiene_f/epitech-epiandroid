package eu.epitech.epiandroid.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class ServiceBase
{
    protected final IApiService apiService = new ApiServiceLoopj();
    protected final GsonBuilder builder = new GsonBuilder();
    protected final Gson gson = builder.setPrettyPrinting().create();

    protected boolean isJSONValid(String JSON_STRING)
    {
        try {
            gson.fromJson(JSON_STRING, Object.class);
            return true;
        } catch(com.google.gson.JsonSyntaxException ex) {
            return false;
        }
    }
}
