package eu.epitech.epiandroid.services;

import android.os.Looper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

/**
 * Implémentation de la classe abstraite ApiService utilisant la librairie
 * LoopJ pour effectuer les requêtes HTTP
 */
public class ApiServiceLoopj extends ApiService
{
    private final AsyncHttpClient syncClient = new SyncHttpClient();
    private final AsyncHttpClient asyncClient = new AsyncHttpClient();

    /**
     * Encapsule l'éxecution des requêtes de type GET sur l'API
     * @param url Route appelée par la requête
     * @param params Paramètres passés lors de l'appel de la route
     * @param responseHandler Classe gérant le retour de l'API
     */
    public void get(String url, HashMap<String, String> params, final ResponseHandler responseHandler)
    {
        RequestParams requestParams = new RequestParams(params);

        this.get(getAbsoluteUrl(url), requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response)
            {
                responseHandler.onSuccess(response != null ? response.toString() : null);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response)
            {
                responseHandler.onSuccess(response != null ? response.toString() : null);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString)
            {
                responseHandler.onSuccess(responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse)
            {
                responseHandler.onFailure(errorResponse != null ? errorResponse.toString() : null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse)
            {
                responseHandler.onFailure(errorResponse != null ? errorResponse.toString() : null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                responseHandler.onFailure(responseString);
            }
        });
    }

    /**
     * Encapsule l'éxecution des requêtes de type POST sur l'API
     * @param url Route appelée par la requête
     * @param params Paramètres passés lors de l'appel de la route
     * @param responseHandler Classe gérant le retour de l'API
     */
    public void post(String url, HashMap<String, String> params, final ResponseHandler responseHandler)
    {
        RequestParams requestParams = new RequestParams(params);

        this.post(getAbsoluteUrl(url), requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                responseHandler.onSuccess(response != null ? response.toString() : null);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                responseHandler.onSuccess(response != null ? response.toString() : null);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                responseHandler.onSuccess(responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                responseHandler.onFailure(errorResponse != null ? errorResponse.toString() : null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                responseHandler.onFailure(errorResponse != null ? errorResponse.toString() : null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                responseHandler.onFailure(responseString);
            }
        });
    }

    /**
     * Encapsule l'éxecution des requêtes de type DELETE sur l'API
     * @param url Route appelée par la requête
     * @param params Paramètres passés lors de l'appel de la route
     * @param responseHandler Classe gérant le retour de l'API
     */
    public void delete(String url, HashMap<String, String> params, final ResponseHandler responseHandler)
    {
        RequestParams requestParams = new RequestParams(params);

        this.delete(getAbsoluteUrl(url), requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                responseHandler.onSuccess(response != null ? response.toString() : null);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                responseHandler.onSuccess(response != null ? response.toString() : null);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                responseHandler.onSuccess(responseString);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                responseHandler.onFailure(errorResponse != null ? errorResponse.toString() : null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                responseHandler.onFailure(errorResponse != null ? errorResponse.toString() : null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                responseHandler.onFailure(responseString);
            }
        });
    }

    private void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
    {
        getClient().get(url, params, responseHandler);
    }

    private void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
    {
        getClient().post(url, params, responseHandler);
    }

    private void delete(String url, RequestParams params, AsyncHttpResponseHandler responseHandler)
    {
        getClient().delete(url, params, responseHandler);
    }

    private AsyncHttpClient getClient()
    {
        if (Looper.myLooper() == null)
            return (syncClient);
        return (asyncClient);
    }
}
