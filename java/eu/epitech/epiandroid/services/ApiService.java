package eu.epitech.epiandroid.services;

public abstract class ApiService implements IApiService
{
    private static final String BASE_URL = "https://epitech-api.herokuapp.com/";

    protected String getAbsoluteUrl(String relativeUrl)
    {
        return (BASE_URL + relativeUrl);
    }
}
