package eu.epitech.epiandroid.services;

import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import eu.epitech.epiandroid.handlers.PlanningEventsHandler;
import eu.epitech.epiandroid.models.Event;

public class PlanningService extends ServiceBase
{
    final private PlanningEventsHandler planningEventsHandler;

    public PlanningService(PlanningEventsHandler planningEventsHandler)
    {
        this.planningEventsHandler = planningEventsHandler;
    }

    public void eventList()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_WEEK, 1);

        String dateStart = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

        calendar.set(Calendar.DAY_OF_WEEK, 7);
        String dateEnd = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());

        this.eventList(dateStart, dateEnd);
    }

    public void eventList(String dateStart, String dateEnd)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("start", dateStart);
        params.put("end", dateEnd);

        apiService.get("/planning", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    Event[] events = gson.fromJson(response, Event[].class);

                    planningEventsHandler.onEventListSuccess(events);
                } catch (Exception e) {
                    planningEventsHandler.onEventListError("Error while processing json: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response) {
                if (isJSONValid(response)) {
                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(response).getAsJsonObject();
                        JsonObject error = obj.getAsJsonObject("error");
                        String message = error.getAsJsonPrimitive("message").getAsString();
                        planningEventsHandler.onEventListError(message);
                    } catch (Exception e) {
                        planningEventsHandler.onEventListError(response);
                    }
                } else
                    planningEventsHandler.onEventListError(response);
            }
        });
    }

    public void subscribe(final Event event)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("scolaryear", Integer.toString(event.getScolarYear()));
        params.put("codemodule", event.getModuleCode());
        params.put("codeinstance", event.getInstanceCode());
        params.put("codeacti", event.getActivityCode());
        params.put("codeevent", event.getEventCode());

        apiService.post("/event", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                planningEventsHandler.onEventSubscribeSuccess(event);
            }

            @Override
            public void onFailure(String response) {
                if (isJSONValid(response)) {
                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(response).getAsJsonObject();
                        JsonObject error = obj.getAsJsonObject("error");
                        String message = error.getAsJsonPrimitive("message").getAsString();
                        planningEventsHandler.onEventSubscribeError(message);
                    } catch (Exception e) {
                        planningEventsHandler.onEventSubscribeError(response);
                    }
                } else
                    planningEventsHandler.onEventSubscribeError(response);
            }
        });
    }

    public void unsubscribe(final Event event)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("scolaryear", Integer.toString(event.getScolarYear()));
        params.put("codemodule", event.getModuleCode());
        params.put("codeinstance", event.getInstanceCode());
        params.put("codeacti", event.getActivityCode());
        params.put("codeevent", event.getEventCode());

        apiService.delete("/event", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                planningEventsHandler.onEventUnsubscribeSuccess(event);
            }

            @Override
            public void onFailure(String response) {
                if (isJSONValid(response))
                {
                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(response).getAsJsonObject();
                        JsonObject error = obj.getAsJsonObject("error");
                        String message = error.getAsJsonPrimitive("message").getAsString();
                        planningEventsHandler.onEventUnsubscribeError(message);
                    } catch (Exception e)
                    {
                        planningEventsHandler.onEventUnsubscribeError(response);
                    }
                } else
                    planningEventsHandler.onEventUnsubscribeError(response);
            }
        });
    }

    public void submitToken(final Event event, final String token)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("scolaryear", Integer.toString(event.getScolarYear()));
        params.put("codemodule", event.getModuleCode());
        params.put("codeinstance", event.getInstanceCode());
        params.put("codeacti", event.getActivityCode());
        params.put("codeevent", event.getEventCode());
        params.put("tokenvalidationcode", token);

        apiService.post("/token", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response)
            {
                try
                {
                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();
                    JsonPrimitive error = obj.getAsJsonPrimitive("error");

                    if (error != null)
                        planningEventsHandler.onTokenSubmitError(error.getAsString());
                    else
                        planningEventsHandler.onTokenSubmitSuccess(event);
                } catch (Exception e)
                {
                    planningEventsHandler.onTokenSubmitError("Error while processing json: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response) {
                if (isJSONValid(response))
                {
                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(response).getAsJsonObject();
                        JsonObject error = obj.getAsJsonObject("error");
                        String message = error.getAsJsonPrimitive("message").getAsString();
                        planningEventsHandler.onTokenSubmitError(message);
                    } catch (Exception e)
                    {
                        planningEventsHandler.onTokenSubmitError(response);
                    }
                } else
                    planningEventsHandler.onTokenSubmitError(response);
            }
        });
    }
}
