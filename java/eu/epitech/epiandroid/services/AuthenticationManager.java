package eu.epitech.epiandroid.services;

import android.content.Context;
import android.content.SharedPreferences;

import eu.epitech.epiandroid.models.User;

public class AuthenticationManager
{
    private static final String PREFERENCES_KEY = "EpiAndroid";
    private static AuthenticationManager.Token token = null;
    private static String login = null;
    private static String password = null;
    private static User user = null;
    private static SharedPreferences sharedPreferences;

    public static void loadContext(Context context)
    {
        sharedPreferences = context.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE);

        AuthenticationManager.getLogin();
        AuthenticationManager.getPassword();
    }

    public static boolean hasToken()
    {
        return (token != null);
    }

    public static Token getToken()
    {
        return (token);
    }

    public static void setToken(Token token)
    {
        AuthenticationManager.token = token;
    }

    public static boolean hasCredentials()
    {
        return (login != null && password != null);
    }

    public static void saveCredentials(String login, String password)
    {
        AuthenticationManager.login = login;
        AuthenticationManager.password = password;

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("login", login);
        editor.putString("password", password);
        editor.commit();
    }

    public static String getLogin()
    {
        if (login == null)
            login = sharedPreferences.getString("login", null);

        return (login);
    }

    public static String getPassword()
    {
        if (password == null)
            password = sharedPreferences.getString("password", null);

        return (password);
    }

    public static User getUser()
    {
        return (user);
    }

    public static void setUser(User user)
    {
        AuthenticationManager.user = user;
    }

    public class Token
    {
        private String token;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        @Override
        public String toString()
        {
            return (token);
        }
    }

}
