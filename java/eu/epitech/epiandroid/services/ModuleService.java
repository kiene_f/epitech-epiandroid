package eu.epitech.epiandroid.services;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;

import eu.epitech.epiandroid.handlers.ModuleEventsHandler;
import eu.epitech.epiandroid.models.Module;

public class ModuleService extends ServiceBase
{
    private final ModuleEventsHandler moduleEventsHandler;

    public ModuleService(ModuleEventsHandler moduleEventsHandler)
    {
        this.moduleEventsHandler = moduleEventsHandler;
    }

    /**
     * Récupère la liste des modules auxquels l'utilisateur est inscrit
     */
    public void selfModuleList()
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());

        apiService.get("/modules", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();
                    JsonArray items = obj.getAsJsonArray("modules");

                    Module[] modules = gson.fromJson(items, Module[].class);
                    for (Module module : modules)
                        module.setStudentRegistered(true);

                    moduleEventsHandler.onSelfModuleListSuccess(modules);
                } catch (Exception e) {
                    moduleEventsHandler.onSelfModuleListError("Error while processing json: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response) {
                try {
                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();
                    JsonObject error = obj.getAsJsonObject("error");
                    String message = error.getAsJsonPrimitive("message").getAsString();
                    moduleEventsHandler.onSelfModuleListError(message);
                } catch (Exception e) {
                    moduleEventsHandler.onSelfModuleListError(response);
                }
            }
        });
    }

    /**
     * Récupère la liste des modules disponibles
     * @param scolarYear Année scolaire pour laquelle les modules seront récupérés
     * @param location Ville pour laquelle les modules seront récupérés
     */
    public void moduleList(int scolarYear, String location)
    {
        this.moduleList(scolarYear, location, "bachelor/classic");
    }

    /**
     * Récupère la liste des modules disponibles
     * @param scolarYear Année scolaire pour laquelle les modules seront récupérés
     * @param location Ville pour laquelle les modules seront récupérés
     * @param course Cycle pour lequel les modules seront récupérés
     */
    public void moduleList(int scolarYear, String location, String course)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("scolaryear", Integer.toString(scolarYear));
        params.put("location", location);
        params.put("course", course);

        apiService.get("/allmodules", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    JsonParser parser = new JsonParser();
                    JsonObject obj = parser.parse(response).getAsJsonObject();
                    JsonArray items = obj.getAsJsonArray("items");

                    Module[] modules = gson.fromJson(items, Module[].class);

                    moduleEventsHandler.onModuleListSuccess(modules);
                } catch (Exception e) {
                    moduleEventsHandler.onModuleListError("Error while processing json: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response) {
                if (response != null) {
                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(response).getAsJsonObject();
                        JsonObject error = obj.getAsJsonObject("error");
                        String message = error.getAsJsonPrimitive("message").getAsString();
                        moduleEventsHandler.onModuleListError(message);
                    } catch (Exception e) {
                        moduleEventsHandler.onModuleListError(response);
                    }
                } else
                    moduleEventsHandler.onModuleListError("error");
            }
        });
    }

    public void subscribe(final Module module)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("scolaryear", Integer.toString(module.getScolarYear()));
        params.put("codemodule", module.getModuleCode());
        params.put("codeinstance", module.getInstanceCode());

        System.out.println(module.getModuleCode());
        apiService.post("/module", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    moduleEventsHandler.onModuleSubscribeSuccess(module);
                } catch (Exception e) {
                    moduleEventsHandler.onModuleSubscribeError("Error while processing json: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response) {
                if (isJSONValid(response))
                {
                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(response).getAsJsonObject();
                        JsonObject error = obj.getAsJsonObject("error");
                        String message = error.getAsJsonPrimitive("message").getAsString();
                        moduleEventsHandler.onModuleSubscribeError(message);
                    } catch (Exception e)
                    {
                        moduleEventsHandler.onModuleSubscribeError(response);
                    }
                } else
                    moduleEventsHandler.onModuleSubscribeError(response);
            }
        });
    }

    public void unsubscribe(final Module module)
    {
        HashMap<String, String> params = new HashMap<>();
        params.put("token", AuthenticationManager.getToken().toString());
        params.put("scolaryear", Integer.toString(module.getScolarYear()));
        params.put("codemodule", module.getModuleCode());
        params.put("codeinstance", module.getInstanceCode());

        apiService.delete("/module", params, new ResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    moduleEventsHandler.onModuleUnsubscribeSuccess(module);
                } catch (Exception e) {
                    moduleEventsHandler.onModuleUnsubscribeError("Error while processing json: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(String response) {
                if (isJSONValid(response))
                {
                    try {
                        JsonParser parser = new JsonParser();
                        JsonObject obj = parser.parse(response).getAsJsonObject();
                        JsonObject error = obj.getAsJsonObject("error");
                        String message = error.getAsJsonPrimitive("message").getAsString();
                        moduleEventsHandler.onModuleUnsubscribeError(message);
                    } catch (Exception e)
                    {
                        moduleEventsHandler.onModuleUnsubscribeError(response);
                    }
                } else
                    moduleEventsHandler.onModuleUnsubscribeError(response);
            }
        });
    }
}
