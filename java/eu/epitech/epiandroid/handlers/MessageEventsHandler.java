package eu.epitech.epiandroid.handlers;

import eu.epitech.epiandroid.models.Message;

public interface MessageEventsHandler
{
    void onMessageListSuccess(Message[] messages);
    void onMessageListError(String error);
}
