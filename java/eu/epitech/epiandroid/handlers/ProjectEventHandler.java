package eu.epitech.epiandroid.handlers;

import eu.epitech.epiandroid.models.Project;

public interface ProjectEventHandler
{
    void onProjectListSuccess(Project[] projects);
    void onProjectlistError(String error);
    void onProjectSubscribeSuccess(Project project);
    void onProjectSubscribeError(String error);
    void onProjectUnsubscribeSuccess(Project event);
    void onProjectUnsubscribeError(String error);
}
