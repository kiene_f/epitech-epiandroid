package eu.epitech.epiandroid.handlers;

import eu.epitech.epiandroid.models.Module;

public interface ModuleEventsHandler
{
    void onSelfModuleListSuccess(Module[] modules);
    void onSelfModuleListError(String error);
    void onModuleListSuccess(Module[] modules);
    void onModuleListError(String error);
    void onModuleSubscribeSuccess(Module module);
    void onModuleSubscribeError(String error);
    void onModuleUnsubscribeSuccess(Module module);
    void onModuleUnsubscribeError(String error);
}
