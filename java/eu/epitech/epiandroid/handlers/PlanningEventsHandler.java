package eu.epitech.epiandroid.handlers;

import eu.epitech.epiandroid.models.Event;

public interface PlanningEventsHandler
{
    void onEventListSuccess(Event[] events);
    void onEventListError(String error);
    void onEventSubscribeSuccess(Event event);
    void onEventSubscribeError(String error);
    void onEventUnsubscribeSuccess(Event event);
    void onEventUnsubscribeError(String error);
    void onTokenSubmitSuccess(Event event);
    void onTokenSubmitError(String error);
}
