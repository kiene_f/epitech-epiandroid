package eu.epitech.epiandroid.handlers;

import eu.epitech.epiandroid.services.AuthenticationManager;

public interface AuthEventsHandler
{
    void onLoginSuccess(AuthenticationManager.Token token);
    void onLoginError(String error);
}
