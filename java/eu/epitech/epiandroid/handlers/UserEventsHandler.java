package eu.epitech.epiandroid.handlers;

import eu.epitech.epiandroid.models.User;

public interface UserEventsHandler
{
    void onSelfInfosSuccess(User user);
    void onSelfInfosError(String error);
    void onInfosSuccess(User user);
    void onInfosError(String error);
    void onPhotoSuccess(String photo);
    void onPhotoError(String error);
    void onTrombiSuccess(User[] users);
    void onTrombiError(String error);
}
