# Epitech - EpiAndroid #
### French
EpiAndroid est un projet de 3e année qui a pour but le portage de notre intranet sur mobile. En se basant sur une api, nous devions effectuer toutes les actions que notre intranet nous permet.

![alt text](http://francois.kiene.fr/assets/images/works/epiandroid.jpg)

## Built With

* Java
* Lombok